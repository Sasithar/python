i = 5
while i >= 1:
    j = 1
    while j <= i:
        if j% 2 == 0:
            print("* ", end="")
        else:
            print("#", end=" ")
        j += 1
    print()
    i -= 1
